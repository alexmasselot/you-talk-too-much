import * as d3 from 'd3';

export default class GaugeD3 {
  container: any;

  configuration = {
    size: 200,
    clipWidth: 200,
    clipHeight: 110,
    ringInset: 20,
    ringWidth: 20,

    pointerWidth: 10,
    pointerTailLength: 5,
    pointerHeadLengthPercent: 0.9,

    minValue: 0,
    maxValue: 10,

    minAngle: -90,
    maxAngle: 90,

    transitionMs: 750,

    majorTicks: 5,
    labelFormat: d3.format(',g'),
    labelInset: 10,

    minLabel: '',
    maxLabel: '',

    arcColorFn: d3.interpolateHsl(d3.rgb('#e8e2ca'), d3.rgb('#3e6c0a')),
  };

  range: number;

  r: number;

  pointerHeadLength: number;

  svg: any;

  arc: any;

  scale: any;

  tickData: number[];

  pointer: any;

  constructor(container: any, config: any = {}) {
    this.container = container;
    this.configuration = { ...this.configuration, ...config };
    this.configuration.clipWidth = this.configuration.size;
    this.configuration.clipHeight = (this.configuration.size / 2) * 1.1;

    this.range = this.configuration.maxAngle - this.configuration.minAngle;
    this.r = this.configuration.size / 2;
    this.pointerHeadLength = Math.round(this.r * this.configuration.pointerHeadLengthPercent);

    // a linear scale that maps domain values to a percent from 0..1
    this.scale = d3.scaleLinear()
      .range([0, 1])
      .domain([this.configuration.minValue, this.configuration.maxValue]);

    this.tickData = d3.range(this.configuration.majorTicks).map(() => 1
      / this.configuration.majorTicks);

    this.arc = d3.arc()
      .innerRadius(this.r - this.configuration.ringWidth - this.configuration.ringInset)
      .outerRadius(this.r - this.configuration.ringInset)
      .startAngle((d: any, i: any) => {
        const ratio = d * i;
        return this.deg2rad(this.configuration.minAngle + (ratio * this.range));
      })
      .endAngle((d: any, i: any) => {
        const ratio = d * (i + 1);
        return this.deg2rad(this.configuration.minAngle + (ratio * this.range));
      });
  }

  // eslint-disable-next-line class-methods-use-this
  deg2rad(deg: number): number {
    return (deg / 180) * Math.PI;
  }

  newAngle(d: number): number {
    const ratio = this.scale(d);
    return this.configuration.minAngle + (ratio * this.range);
  }

  centerTranslation() {
    return `translate(${this.r},${this.r})`;
  }

  isRendered() {
    return (this.svg !== undefined);
  }

  update(newValue: number) {
    const ratio = this.scale(newValue);
    const na = this.configuration.minAngle + (ratio * this.range);
    this.pointer
      .attr('transform', `rotate(${na})`);
  }

  render(newValue: number) {
    const labelHeight = 20;
    this.svg = d3.select(this.container)
      .append('svg:svg')
      .attr('class', 'gauge')
      .attr('width', this.configuration.clipWidth)
      .attr('height', this.configuration.clipHeight + labelHeight);

    const centerTx = this.centerTranslation();

    const arcs = this.svg.append('g')
      .attr('class', 'arc')
      .attr('transform', centerTx);

    arcs.selectAll('path')
      .data(this.tickData)
      .enter().append('path')
      .attr('fill', (d: any, i: any) => this.configuration.arcColorFn(d * i))
      .attr('d', this.arc);

    const lg = this.svg.append('g')
      .attr('class', 'label')
      .attr('transform', `translate(${this.configuration.clipWidth
      / 2},${this.configuration.clipHeight})`);
    const labelShift = (this.configuration.clipWidth / 2)
      - this.configuration.ringInset
      - (this.configuration.ringWidth / 2);
    lg.append('g')
      .attr('transform', `translate(-${labelShift},0)`)
      .append('text')
      .text(this.configuration.minLabel);
    lg.append('g')
      .attr('transform', `translate(${labelShift},0)`)
      .append('text')
      .text(this.configuration.maxLabel);

    const lineData = [[this.configuration.pointerWidth / 2, 0],
      [0, -this.pointerHeadLength],
      [-(this.configuration.pointerWidth / 2), 0],
      [0, this.configuration.pointerTailLength],
      [this.configuration.pointerWidth / 2, 0]];
    const pointerLine = d3.line();

    const pg = this.svg.append('g')
      .data([lineData])
      .attr('class', 'pointer')
      .attr('transform', centerTx);

    this.pointer = pg.append('path')
      .attr('d', pointerLine/* function(d) { return pointerLine(d) +'Z';} */)
      .attr('transform', `rotate(${this.configuration.minAngle})`);

    this.update(newValue || 0);
  }
}
