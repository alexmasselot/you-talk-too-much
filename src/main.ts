import Vue from 'vue';
import Buefy from 'buefy';
import { library } from '@fortawesome/fontawesome-svg-core';

import {
  faMicrophone,
  faUpload,
  faDownload,
  faEllipsisH,
  faWindowClose,
  faVenus,
  faTimes,
  faMars,
  faCog,
  faCogs,
} from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueGtag from 'vue-gtag';
import App from './App.vue';
import store from './store';
import router from './router';
import 'buefy/dist/buefy.css';

Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
});

library.add(
  faMicrophone,
  faUpload,
  faDownload,
  faEllipsisH,
  faWindowClose,
  faTimes,
  faMars,
  faVenus,
  faCog,
  faCogs,
);
Vue.component('vue-fontawesome', FontAwesomeIcon);

Vue.config.productionTip = false;

Vue.use(VueGtag, {
  config: { id: 'G-467HM1Q4E4' },
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
