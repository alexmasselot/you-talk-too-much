import * as _ from 'lodash';
import Meeting from '@/models/meeting';

export function deviationCoefficient(values: number[]): number {
  if (values.length <= 1) {
    return 0;
  }
  const total = _.sum(values);
  if (total === 0) {
    return 0;
  }
  const nb = values.length;
  const relativeDurations = values.map((d) => d / total);
  const perfectRatio = 1 / nb;
  const deviationFromRatio = _.chain(relativeDurations)
    .map((t) => Math.abs(t - perfectRatio))
    .sum()
    .value();

  return (deviationFromRatio * nb) / (nb + 1);
}

export function teamLanguageCoefficient(meeting: Meeting): number {
  const durations = Object.values(meeting.talkerTotalContribution());
  return deviationCoefficient(durations);
}

export function genderLanguageCoefficient(meeting: Meeting): number {
  const durations = Object.values(meeting.genderContributionRatio());
  return deviationCoefficient(durations);
}
