import * as _ from 'lodash';
import { VttRecord } from '@/models/vtt-record';
import Intervention from '@/models/intervention';
import Meeting from '@/models/meeting';
import processingConfiguration from '@/services/processingConfiguration';

/**
 * from a list of record for the same talker, packed the record is they are close enough
 * @param vttRecords
 */
function packVttRecordsForSingleTalker(vttRecords: VttRecord[]): VttRecord[] {
  const reducer = (acc: VttRecord[], record: VttRecord): VttRecord[] => {
    if (acc.length === 0) {
      return [record];
    }
    const lastRecord = acc.pop();
    if (lastRecord) {
      if (record.timeStart <= lastRecord.timeEnd + processingConfiguration.pauseDurationThreshold) {
        acc.push(new VttRecord(lastRecord.timeStart, record.timeEnd, lastRecord.talker));
      } else {
        acc.push(lastRecord, record);
      }
    }
    return acc;
  };
  return vttRecords.reduce(reducer, []);
}

function removeShortRecords(vttRecords: VttRecord[]): VttRecord[] {
  return vttRecords.filter((record) => (record.timeEnd - record.timeStart)
    >= processingConfiguration.interventionDurationThreshold);
}

/**
 * from a list of VttRecords, returns a list of such (sorted by timeStart), where
 * * adjacent record for a single talker are packed
 *   (whithin a pause threshold provided through configuration)
 * * interventions shorter than threshold are removed
 * @param vttRecords
 */
export function packVttRecords(vttRecords: VttRecord[]): VttRecord[] {
  return _.chain(vttRecords)
    .sortBy('timeStart')
    .groupBy((record) => record.talker)
    .values()
    .map(packVttRecordsForSingleTalker)
    .map(removeShortRecords)
    .flatten()
    .sortBy('timeStart')
    .value();
}

/**
 * transform a list of vtt records into interventions
 * @param vttRecords
 */
export function packInterventions(vttRecords: VttRecord[]): Intervention[] {
  const records = packVttRecords(vttRecords);

  const initRecord = records.shift();
  if (!initRecord) {
    return [];
  }

  const reducer = (
    acc: {
      anchor: VttRecord,
      list: Intervention[],
      previous: {
        talker: string | null,
        timeEnd: number
      }
    },
    record: VttRecord,
  ): {
    anchor: VttRecord, list: Intervention[],
    previous: {
      talker: string | null,
      timeEnd: number
    }
  } => {
    // same is talking
    if (acc.anchor.talker === record.talker) {
      return {
        anchor: new VttRecord(acc.anchor.timeStart, record.timeEnd, acc.anchor.talker),
        list: acc.list,
        previous: acc.previous,
      };
    }
    // new talker
    const newIntervention = new Intervention(
      acc.anchor.timeStart,
      acc.anchor.timeEnd,
      acc.anchor.talker,
      acc.previous.timeEnd > acc.anchor.timeStart,
      acc.previous.talker,
      record.timeStart < acc.anchor.timeEnd,
      record.talker,
    );
    return {
      anchor: record,
      list: [...acc.list, newIntervention],
      previous: { talker: acc.anchor.talker, timeEnd: acc.anchor.timeEnd },
    };
  };

  const reduced = records.reduce(reducer, {
    anchor: initRecord as VttRecord,
    list: [] as Intervention[],
    previous: {
      talker: null,
      timeEnd: -1,
    },
  });

  const previousTalker = reduced.previous.talker;
  const isInterrupting = reduced.previous.timeEnd > reduced.anchor.timeStart;
  const lastIntervention = new Intervention(
    reduced.anchor.timeStart,
    reduced.anchor.timeEnd,
    reduced.anchor.talker,
    isInterrupting,
    previousTalker,
    false,
    null,
  );
  return [...reduced.list, lastIntervention];
}

export default function buildMeeting(vttRecords: VttRecord[]): Meeting {
  const interventions = packInterventions(vttRecords);
  return new Meeting(interventions);
}
