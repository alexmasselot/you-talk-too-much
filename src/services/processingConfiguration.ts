export default {
  interventionDurationThreshold: 1,
  pauseDurationThreshold: 0.5,
};
