import Intervention from '@/models/intervention';

import Meeting from '@/models/meeting';
import { teamLanguageCoefficient } from '@/services/deviation-coefficient';

describe('team-language-coefficient', () => {
  it('one talker', () => {
    const interventions = [
      new Intervention(0, 12, 'A', false, null, false, 'B'),
      new Intervention(12, 12, 'B', false, 'A', false, 'A'),
      new Intervention(12, 12, 'C', false, 'B', false, null),
    ];
    const meeting = new Meeting(interventions);

    const got = teamLanguageCoefficient(meeting);

    expect(got).toEqual(1);
  });

  it('perfect balance', () => {
    const interventions = [
      new Intervention(0, 4, 'A', false, null, false, 'B'),
      new Intervention(4, 8, 'B', false, 'A', false, 'A'),
      new Intervention(8, 12, 'C', false, 'B', false, null),
    ];
    const meeting = new Meeting(interventions);

    const got = teamLanguageCoefficient(meeting);

    expect(got).toEqual(0);
  });

  it('2 50%, 10%', () => {
    const interventions = [
      new Intervention(0, 6, 'A', false, null, false, 'B'),
      new Intervention(6, 12, 'B', false, 'A', false, 'A'),
      new Intervention(12, 12, 'C', false, 'B', false, null),
    ];
    const meeting = new Meeting(interventions);

    const got = teamLanguageCoefficient(meeting);

    expect(got).toEqual(0.5);
  });
});
