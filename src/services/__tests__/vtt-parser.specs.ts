import VttParser from '@/services/vtt-parser';
import { VttRecord } from '@/models/vtt-record';

const content = `WEBVTT

00:00:00.000 --> 00:00:01.120
<v Dingo>You might hear my sidewalk.</v>

00:00:01.570 --> 00:00:03.570
<v Dingo>Is he passing meeting bipolar?</v>

00:00:04.820 --> 00:00:05.740
<v Dingo>You are doing fine.</v>

00:00:06.150 --> 00:00:08.280
<v Dingo>Artistic wire on the X the the the.</v>

00:00:09.570 --> 00:00:11.600
<v Dingo>Marketa, right, so let's say about.</v>

00:00:11.740 --> 00:00:13.140
<v Dingo>And keep him on his back.</v>

00:00:13.540 --> 00:00:14.270
<v Dingo>Lucy, Alex</v>

00:00:14.650 --> 00:00:15.390
<v Dingo>a certain percentage.</v>

00:00:15.780 --> 00:00:16.190
<v Dingo>His lips.</v>

00:00:16.410 --> 00:00:18.380
<v Mickey>Why don't we just?</v>

00:00:18.620 --> 00:00:24.390
<v Mickey>So the preview the measure a corner metric person.</v>

00:00:26.970 --> 00:00:27.620
<v Minnie>Why so coming?</v>

00:00:27.910 --> 00:00:29.250
<v Minnie>Is effecting lady?</v>

00:00:30.560 --> 00:00:31.590
<v Dingo>To disable, yeah.</v>

00:00:31.680 --> 00:00:41.320
<v Mickey>Would you like to look for Lego? Uh, I would approval by the doctor for someone.</v>

00:00:41.370 --> 00:00:44.950
<v Mickey>Point in opening, uh, it might do norm data.</v>

00:00:46.850 --> 00:00:47.480
<v Mickey>Jerry</v>`;

describe('vtt-parser', () => {
  const parser = new VttParser(content);
  it('blocks', () => {
    const got = parser.textBlocks();
    expect(got).toHaveLength(17);
    expect(got[2]).toEqual('00:00:04.820 --> 00:00:05.740\n<v Dingo>You are doing fine.</v>');
  });

  it('parseTime', () => {
    const txt = '02:07:04.820';

    const got = VttParser.parseTime(txt);

    expect(got).toEqual(4.820 + 7 * 60 + 2 * 3600);
  });

  it('parseBlock', () => {
    const txt = '00:00:04.820 --> 00:00:05.740\n'
      + '<v Dingo>You are doing fine.</v>';

    const got = VttParser.parseBlock(txt);

    expect(got).toEqual(new VttRecord(4.820, 5.740, 'Dingo'));
  });
});
