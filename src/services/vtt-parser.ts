import moment from 'moment';
import { VttRecord } from '@/models/vtt-record';

const reRecord = new RegExp('(.*) \\-\\-> (.*)\\n<v (.*)>.*</v>');
export default class VttParser {
  private readonly content: string;

  constructor(content: string) {
    this.content = content
      .replace(/\r/g, '')
      .replace('WEBVTT\n\n', '');
  }

  textBlocks(): string[] {
    return this.content.split(/\n\n/);
  }

  static parseTime(str: string): number {
    const mom = moment(str, 'HH:mm:ss.SSS');
    return mom.hours() * 3600 + mom.minutes() * 60 + mom.seconds() + mom.milliseconds() / 1000;
  }

  static parseBlock(block: string): VttRecord {
    const match = block.match(reRecord);
    if (!match) {
      throw new Error(`cannot parse record ${block}`);
    }
    return new VttRecord(
      VttParser.parseTime(match[1]),
      VttParser.parseTime(match[2]),
      match[3],
    );
  }

  records(): VttRecord[] {
    return this.textBlocks()
      .filter((txt) => txt.trim() !== '')
      .map((txt) => VttParser.parseBlock(txt));
  }
}
