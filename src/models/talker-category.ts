import Gender from '@/models/gender';

export default class TalkerCategory {
  gender: Gender | undefined;
}
