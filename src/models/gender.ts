// eslint-disable-next-line no-shadow
enum Gender {
  FEMALE = 'female',
  NON_BINARY = 'non-binary',
  MALE = 'male'
}

export default Gender;

export const genderOrder = [Gender.FEMALE, Gender.NON_BINARY, Gender.MALE, undefined];

export const genderIcons = {
  [Gender.FEMALE]: 'venus',
  [Gender.MALE]: 'mars',
  [Gender.NON_BINARY]: 'times',
};
